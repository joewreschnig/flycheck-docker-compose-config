# flycheck-docker-compose-config

[![pipeline status](https://gitlab.com/joewreschnig/flycheck-docker-compose-config/badges/master/pipeline.svg)](https://gitlab.com/joewreschnig/flycheck-docker-compose-config/pipelines)

Check [Docker Compose files][] with `docker-compose config` when using
[Flycheck][] in [`docker-compose-mode`][docker-compose-mode].

To enable it, call `flycheck-docker-compose-config-enable`. A recommend
deferred [use-package][] stanza is:

```elisp
(use-package flycheck-docker-compose-config
  :demand :after flycheck docker-compose-mode
  :config
  (flycheck-docker-compose-config-enable))
```

[Docker Compose files]: https://docs.docker.com/compose/compose-file/
[Flycheck]: http://www.flycheck.org/
[docker-compose-mode]: https://github.com/meqif/docker-compose-mode
[use-package]: https://github.com/jwiegley/use-package


## Known Issues

Docker Compose messages only includes line numbers when encountering a
parsing error. Other errors are specified inconsistently, usually by
YAML key path or parts of a key path. As a consequence, line attribution
for this checker is unreliable.


## License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
