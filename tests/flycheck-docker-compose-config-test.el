;;; flycheck-docker-compose-config-test.el --- -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2017 Joe Wreschnig
;;
;; Author: Joe Wreschnig
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.


;;; Commentary:
;;
;; This file contains test cases for ‘flycheck-docker-compose-config’.  Unless
;; you’re hacking on it you shouldn’t need to edit or run this file.


;;; Code:

(require 'buttercup)
(require 'flycheck-docker-compose-config)

(defmacro with-test-buffer (&rest body)
  "Create a test buffer evaluate BODY with it."
  `(with-temp-buffer
     (insert-file-contents test.yml)
     ,@body))

(put 'message 'safe-local-variable #'stringp)
(put 'position 'safe-local-variable #'consp)

(defmacro it-attributes-the-error-in (filename)
  "Test that the linter can attribute the error in FILENAME.

The error message and expected point position are specified in
file-local variables."
  (let ((name (format "attributes the error in %s" filename))
        (fullpath (format "%s/%s" (file-name-directory load-file-name)
                          filename)))
    `(it ,name
       (with-temp-buffer
         (insert-file-contents ,fullpath)
         (hack-local-variables)
         (flycheck-docker-compose-config--goto-message
          (flycheck-error-new) message)
         (expect (line-number-at-pos) :to-equal (car position))
         (expect (current-column) :to-equal (cdr position))))))

(describe "flycheck-docker-compose-config-goto-message"
  (it-attributes-the-error-in "invalid-top-level.yml")
  (it-attributes-the-error-in "service-invalid-type.yml")
  (it-attributes-the-error-in "service-invalid-type-quotes.yml")
  (it-attributes-the-error-in "unsupported-service-config.yml")
  (it-attributes-the-error-in "unsupported-config-option.yml")
  (it-attributes-the-error-in "unset-envvar.yml")
  (it-attributes-the-error-in "invalid-interp.yml")
  )

;;; flycheck-docker-compose-config-test.el ends here
